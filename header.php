<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="https://tilde.team/css/hacker.css">
	<link rel="stylesheet" href="https://tilde.team/css/fork-awesome.css">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<title>The Intentional Journal of the Tildeverse | <?=($title ?? 'open journal for tildes')?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<meta property="og:title" content="the intentional journal of the tildeverse | <?=($title ?? 'open journal for tildes')?>">
	<meta property="og:url" content="https://journal.tildeverse.org<?=$_SERVER['REQUEST_URI']?>">
	<meta property="og:description" content="<?=($desc??($title ?? "the home of an open journal for tildes"))?>">
	<meta property="og:image" content="https://journal.tildeverse.org/apple-icon.png">
	<meta property="og:site_name" content="The Intentional Journal of the Tildeverse">
	<meta property="og:type" content="website">

</head>
<body style="padding-top: 70px;">
<div class="container">
<?php require 'navbar.php'; ?>

