<?php
include __DIR__."/../vendor/autoload.php";
use tildeteam\wiki;

$parser = wiki::factory();
wiki::$bootstrap = wiki::$forkawesome = true;

if (isset($_GET["slug"])) {
    $entry_name = $_GET["slug"];
} elseif (count($_GET) == 1) {
    $entry_name = array_keys($_GET)[0];
} else {
    $entry_name = "";
}

$filename = "{$entry_name}.md";

if (file_exists($filename)) {
    $document = $parser->parse(file_get_contents($filename));
    $yml = $document->getYAML();
    $content = $document->getContent();
    $title = $yml["title"] ?? "invalid";
    $author = htmlspecialchars($yml["author"]);
    $description = "journal entry {$entry_name}: {$yml["title"]} by {$author}";

    include __DIR__."/../header.php";
?>

    <a href=".">&lt; back to list</a>

    <h1>
        <?=$yml["title"]?>
        <?=$yml["published"] ? "" : " <small><em>draft</em></small>" ?>
    </h1>

    <p>author: <?=$author?></p>
    <p>date: <?=$yml["date"]?></p>
    <br />

    <?=$content?>

<?php
} else {
    $title="journal entries";
    $description="a list of tildeverse journal entries.";
    include __DIR__."/../header.php";

    foreach (glob("*.md") as $entry) {
        $entries[] = [
            "slug" => basename($entry, ".md"),
            "yml" => $parser->parse(file_get_contents($entry))->getYAML(),
        ];
    } ?>

    <h1>journal entries</h1>
    <ul>
        <?php foreach ($entries as $entry) { 
            if (!$entry["yml"]["published"]) continue; ?>
            <li>
                <a href="/entries/<?=$entry["slug"]?>">
                    <?=$entry["yml"]["title"]?> by <?=$entry["yml"]["author"]?>
                </a>
            </li>
        <?php } ?>
    </ul>
<?php }

include __DIR__."/../footer.php"; ?>

