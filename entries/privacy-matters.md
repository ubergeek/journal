---
title: "Privacy Matters"
author: "Anton McClure"
date: "2020-03-12"
published: true
---

Originally published at [antonmcclure.com](https://www.antonmcclure.com/privacy-matters.html)

* * *

There's no doubt to many people that we are living in a highly-connected Internet age full of big data. As the amount of big data goes up, so does the importance of protecting people's privacy. After the GDPR took effect in May 2018, sites were required to say when they'd use cookies, and what those cookies do.

However, for many websites, the *cookie consent* banners still [broke the law by making it hard to reject all tracking](https://www.zdnet.com/article/cookie-consent-most-websites-break-law-by-making-it-hard-to-reject-all-tracking/), were [manipulative or completely meaningless](https://techcrunch.com/2019/08/10/most-eu-cookie-consent-notices-are-meaningless-or-manipulative-study-finds/), and sometimes were used to [undermine the user's privacy](https://techcrunch.com/2020/01/10/cookie-consent-tools-are-being-used-to-undermine-eu-privacy-rules-study-suggests/). What do companies do with these cookies and trackers behind the scenes that they might not want us to know about?

## Why do People Use Cookies?

Companies and people use cookies for analytics, ads, security products, and more. This is usually to just find how sites are being used and what content is popular, but it's often for money and maximizing profit off of user data. This data, for some sites, include personal details such as your full name, usernames, home address, email address, Social Security number or National Identification number, passport or Federal ID info, credit/debit card info, date of birth, phone numbers, login details, and much more, along with linkable information such as first or last name alone, country, state, province, city, gender, race, age, job, position, and workplace. What many don't know is how this data can get used if in the wrong hands: to track where users go online and offline to target the user with ads based on their online and offline activities.

Privacy policies exist to let users know how their data gets used, but you'll have to put blind trust in these documents, and hope that nobody is still [violating user privacy](https://medium.com/bestcompany/5-companies-that-have-been-caught-violating-their-customers-privacy-9cfe660ea3eb).

## Solutions

Several browsers such as Google Chrome and Firefox have extensions as well as privacy and security options that you may use to block various trackers, cross-site content, crypto miners, fingerprinters, or even all third-party and first-party cookies. Blocking all third-party and especially first-party cookies, however, may cause websites to break if they're used to store preferences or login info.

Make sure you thoroughly go through Terms of Services and Privacy Policies before signing up for a service. Even then, you can't be sure that companies are going to follow their own terms.

## In the End

There's no doubt that privacy matters in a world full of tracking and surveillance nearly everywhere. We shouldn't have to put up with or end all tracking, but we need to promote the idea and help people understand privacy matters.

* * *

Copyright &copy; 2020, Anton McClure.

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
