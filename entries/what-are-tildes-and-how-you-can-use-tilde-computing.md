---
title: 'What are "Tildes" and How You Can Use "Tilde Computing"'
author: "Anton McClure"
date: "2019-09-12"
published: true
---

Originally published at [antonmcclure.com](https://www.antonmcclure.com/what-are-tildes-and-how-you-can-use-tilde-computing.html)

* * *

## What are Tildes
A "tilde" server is a public access server running any operating system. These servers are hosted in various ways with many different technologies, ranging from a Raspberry Pi at someones home, to powerful dedicated servers in the Cloud, and nearly everything in between.

The first "tilde" known as [tilde.club](https://medium.com/message/tilde-club-i-had-a-couple-drinks-and-woke-up-with-1-000-nerds-a8904f0a2ebf) by Paul Ford, started in 2014, provided shell accounts and basic services for its users. However, it grew quickly leaving the admin unable to handle all the new user requests. Due to its growing waitlist, different servers became available. Many of these are now defunct, but some of them still exist to this day. Many of these tildes were exclusive/limited to members only, and services were mostly or completely intranet-only. While there were efforts between some tildes to combine services, most of their efforts fell flat. One of the successful efforts started with a group of tilde servers making a shared IRC network known as tilde.chat, which grew as more tildes were made and joined. This group eventually became the loose association known as the tildeverse.

## What do Tildes Do Different
The tildeverse allows learning and using UNIX, Linux, BSD, and other operating systems without having to have the system installed on your computers or servers. When members were asked, the common responses were that tildes are free, not for profit, inclusive, everyones welcomed, and is seen as a safe place for pro-foss beliefs and homebrewed tools.

Despite most similar services requiring a paid membership, or being completely paid, the tildeverse offers many things for free with optional donations.

## Introducing: "Tilde as an Example"
"Tilde as an Example" is a planned series that takes a look at one or more tilde servers, and uses them for testing, development, demonstration, and other various uses. This also utilizes tildes to their full potential and incorporates innovative and modern technologies within tilde environments.

Being an open environment, readers are free to try "Tilde as an Example" things for themselves.

## Getting Started with Tilde Computing
Any server can become a tilde server easily. All thats needed is a basic server (running any operating system) and a community of users.

If you find tildes interesting or want to join the tilde community without the required setup, Id recommend joining a server such as "[tilde.pw](https://tilde.pw)" by [neko](https://tilde.pw/~neko), or a different tilde such as "tilde.team" by ben, "thunix" by ubergeek, or any other tilde. These tildes provide many great services, primarily use only free software, and run on powerful servers.

For those who are already a tilde member, what do tildes mean to you?

* * *

Copyright &copy; 2019, Anton McClure.

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
