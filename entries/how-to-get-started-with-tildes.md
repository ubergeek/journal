---
title: "How to Get Started with Tildes"
author: "Anton McClure"
date: "2020-06-30"
published: true
---

Originally published at [antonmcclure.com](https://www.antonmcclure.com/how-to-get-started-with-tildes.html)

* * *

In the article <q>[What are Tildes and How You Can Use Tilde Computing](https://www.antonmcclure.com/what-are-tildes-and-how-you-can-use-tilde-computing.html)</q>, I was mainly focusing on one of the most prominent tilde communities: the tildeverse. However, there are more than the ones on the tildeverse member list, such as [tilde.pw](https://tilde.pw).

If you hadn't signed up for a tilde yet, such as one of the above tildes, feel free to check them out and sign up for one, both, or any other tildes!

## Logging In

Most functions and features on tildes get done over a **S**ecure **Sh**ell (SSH) connection. Many tildes require SSH keys for logging on to their server(s). If you are unfamiliar with what these are, or don't know how to generate one, feel free to check out my guide on [how to make an SSH key pair](https://www.antonmcclure.com/openssh-basics.html). You can connect from virtually any device running Windows 10, macOS, Linux, UNIX, and most UNIX-based/UNIX-like systems using a tool called OpenSSH, a free implementation of the SSH protocol.

The exact method you'll use to log in varies between operating systems. The standard methods for using SSH are as shown below:

**Microsoft Windows 10:**

  1. Open PowerShell (Win+X and select "Windows PowerShell").
  2. Type `ssh domain.tld -l username` and then press the `enter` key.
  3. Type your SSH password or key passphrase if you set one.

Legacy versions of Windows require third-party software, such as [PuTTY](https://www.antonmcclure.com/putty-guide.html).

**Apple macOS (formerly Mac OS X & OS X):**

  1. Open Terminal.
  2. Type `ssh domain.tld -l username` and then press the `enter` key.
  3. Type your SSH password or key passphrase if you set one.

**Linux and other UNIX:**

  1. Open your distro's terminal, or use TTY1-7.
  2. Type `ssh domain.tld -l username` and then press the `enter` key.
  3. Type your SSH password or key passphrase if you set one.

## Creating and Editing Files

Most tildes have multiple text editors and line editors installed. Some of the most popular choices include `nano` and `vim`, and you are free to choose whichever one you like. If you do not have experience with either, then I recommend using `nano` as it tends to be more beginner-friendly and more user-friendly. Nano, by default, lists various common. Key combinations beginning with `^` use your `CTRL` key, and combinations beginning with `M` use your `ALT` key.

For more help with the editors, you may view their manuals with `man nano` or `man vim`.

## Chatting

Tildes typically have multiple ways you can communicate such as

  - IRC
  - Email
  - `talk`

There are more options, but these tend to be the most popular options.

Most tildes come with popular IRC clients such as `weechat` and `irssi`. If you want to be active or participate in other parts of the Tildeverse, feel free to connect to the Tildeverse's IRC network: `tilde.chat`. The commands you'll use are as follows:

**WeeChat:**

`/server add tilde irc.tilde.chat/6697 -ssl -autoconnect`

**IRSSI:**

`/network add tilde`

`/server add -auto -net tilde -ssl -ssl_verify irc.tilde.chat 6697`

## Emailing

While some tildes are member-only, many tildes, especially the newer tildes, allow external emailing. I use `alpine` for emailing on both my VPS and *tilde.pw*. It's a very user-friendly client, provides helpful information about commands and features for if the user needs it, and is easy to set up and customize if the user wishes to do so.

## Conclusion

All tildes have their own set of software and use cases. I run [tilde.pw](https://tilde.pw) with [neko](https://tilde.pw/~neko/). If either are interesting to you, feel free to sign up, and start making the most out of your new tilde account!

If you have any questions, or want to see more articles and possibly guides on different tilde things, comment below or [email me](https://www.antonmcclure.com/#contact)!

I'm also planning on adding a section to my site that is dedicated to the tildeverse and other tildes, including a list of different tilde servers. If you want your tilde in the list, feel free to contact me with the above information.

Have fun tildeing!

* * *

Copyright &copy; 2020, Anton McClure.

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
