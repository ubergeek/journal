# The Intentional Journal of the Tildeverse #

We are an open journal which accepts any tilde-related or non-tilde related entries.

Currently hosted at [journal.tildeverse.org](https://journal.tildeverse.org/).

[Come chat with us! (tilde.chat:6697/#journal)](https://web.tilde.chat/?join=journal) 
![chat badge](https://tilde.chat/badges/badge.php?channel=%23journal)


## deployment notes

this site requires an nginx rewrite to properly server entries:

	location ~* ^/entries/(.+)$ {
		try_files $uri $uri/ /entries/$1;
		include snippets/php.conf;
	}

