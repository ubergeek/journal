<?php include "header.php"; ?>
<div class="jumbotron">
	<h2>the intentional journal of the tildeverse</h2>
	<pre>
 /\/|                           _                              _ 
|/\/_   _____ _ __ ___  ___    (_) ___  _   _ _ __ _ __   __ _| |
   \ \ / / _ \ '__/ __|/ _ \   | |/ _ \| | | | '__| '_ \ / _` | |
    \ V /  __/ |  \__ \  __/   | | (_) | |_| | |  | | | | (_| | |
     \_/ \___|_|  |___/\___|  _/ |\___/ \__,_|_|  |_| |_|\__,_|_|
                             |__/                                
</pre>
	<br>
	<p>an open journal for tilde members</p>
</div>
<p>We are an open journal which accepts any tilde-related or non-tilde related entries.</p>

<p>The Intentional Journal of the Tildeverse is an open journal for members of any tilde box (i.e; <a href="//tilde.town">tilde.town</a>, <a href="//tilde.team">tilde.team</a>,
<a href="//yourtilde.com">yourtilde.com</a>, etc.) to describe research and results of projects. Papers published should be long-formed essays as
opposed to short-form declarations, and can be on any topic of pure interest to the author, as long as there is non-commercial
motivation to explore and learn more about a topic. In addition to publishing tilde members' papers, we also operate as a social
space where members can participate in many ways, including (but not limited to) reviewing papers and providing constructive
criticism, responding to author questions, writing (obviously), or even simply bringing up topics to be researched. Through reviewing
and providing constructive criticism, we hope to provide a mechanism for authors to better themselves or develop their thinking about a topic.</p>

<?php include "footer.php"; ?>
